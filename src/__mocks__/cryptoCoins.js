import {readFileSync} from 'fs';
import path from 'path';

export const cryptos = JSON.parse(
    readFileSync(path.join(__dirname, 'api.json')).toString()
)

export const coins = [
    { codigo: 'USD', nombre: 'Dolar de Estados Unidos' },
    { codigo: 'MXN', nombre: 'Peso Mexicano' },
    { codigo: 'EUR', nombre: 'Euro' },
    { codigo: 'GBP', nombre: 'Libra Esterlina' }
];