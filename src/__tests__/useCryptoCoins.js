import React from 'react';
import axios from 'axios';
import { render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Form from '../components/Formulario';
import {coins, cryptos} from '../__mocks__/cryptoCoins';

const mockAxios = axios;
const saveCoins = jest.fn();
const saveCryptoCoins = jest.fn();

test('<useCryptoCoins>', async() => {

    //get Data
    mockAxios.get = jest.fn().mockResolvedValue({
        data: cryptos
    })

    render(
        <Form 
            guardarMoneda={saveCoins}
            guardarCriptomoneda={saveCryptoCoins}
        />
    );

    //check options number
    const dropdownCoin = screen.getByTestId('select-coins');
    expect(dropdownCoin.children.length).toEqual(coins.length + 1);

    //check option number from api
    const options =  screen.findAllByTestId('option-crypto');
    expect(await options).toHaveLength(10);
    expect(mockAxios.get).toHaveBeenCalled();
    expect(mockAxios.get).toHaveBeenCalledTimes(1);

    //choose bitcoin and usd
    userEvent.selectOptions(screen.getByTestId('select-coins'), 'USD');
    userEvent.selectOptions(screen.getByTestId('select-crypto'), 'BTC');

    //Simulate a submit
    userEvent.click(screen.getByTestId('submit'));

    //check if function was called
    // expect(guardarMoneda).toHaveBeenCalled();
    // expect(guardarMoneda).toHaveBeenCalledTimes(1);

    expect(saveCoins).toHaveBeenCalled();
    expect(saveCryptoCoins).toHaveBeenCalledTimes(1);
})